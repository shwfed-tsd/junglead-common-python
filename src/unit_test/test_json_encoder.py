# -*- encoding: utf-8 -*-
"""
@File    :   test_json_encoder.py
@Time    :   2021/09/27 12:31:43
@Author  :   Gary.Wang
@Version :   1.0
@Contact :   wangyijun@shwfed.com
@License :   (C)Copyright 1990 - 2021, shwfed.com
@Desc    :   json编码器的单元测试
"""

# here put the import lib
from datetime import datetime
import unittest
import json

from junglead.model import ExtendDecimal, ExtendDatetime
from junglead import JsonEncoder


class SubDemo:
    def __init__(self, id: int = 1):
        self.id = id
        self.dt = ExtendDatetime.now(date_format='%Y%m%d')


class Demo:
    def __init__(self, dt: ExtendDatetime = None, dc: ExtendDecimal = None):
        self.dt = dt
        self.dc = dc
        self.sub_demo = SubDemo()


class TestJsonEncoder(unittest.TestCase):

    def test_json(self):
        _origin = datetime(2021, 10, 1, 10, 0, 0)
        _dt = ExtendDatetime.convert(_origin)
        _dc = ExtendDecimal('0.1112345678', 6)
        _demo = Demo(dt=_dt, dc=_dc)
        print(json.dumps(_demo.__dict__, ensure_ascii=False, cls=JsonEncoder))

        # _data = '{"dt": "2021-10-12 15:25:41", "dc": "0.111235123", "sub_demo": {\"id:\":1, \"dt\": \"2021\"}}'
        # _obj = json.loads(_data)
        # print(_obj)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    # suite.addTest(test_util('test_logger'))
    # suite.addTest(test_util('test_datetime'))
    suite.addTest(TestJsonEncoder('test_json'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
