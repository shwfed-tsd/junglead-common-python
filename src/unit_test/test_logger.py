# -*- encoding: utf-8 -*-
"""
@File    :   test_logger.py
@Time    :   2021/09/30 09:35:58
@Author  :   Gary.Wang
@Version :   1.0
@Contact :   wangyijun@shwfed.com
@License :   (C)Copyright 1990 - 2021, shwfed.com
@Desc    :   日志工具的单元测试
"""

# here put the import lib
import unittest

from junglead import LoggerUtil, LoggerLevel


class TestLogger(unittest.TestCase):

    def test(self):
        # LoggerUtil(111).logger.error("正常信息")
        LoggerUtil(LoggerLevel.ERROR).logger.info("错误信息")
        # LoggerUtil(prefix='mq').logger.info('带有前缀的日志记录')


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(TestLogger('test'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
