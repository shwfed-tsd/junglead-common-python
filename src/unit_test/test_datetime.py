# -*- encoding: utf-8 -*-
"""
@File    :   test_datetime.py
@Time    :   2021/09/30 17:05:04
@Author  :   Gary.Wang
@Version :   1.0
@Contact :   wangyijun@shwfed.com
@License :   (C)Copyright 1990 - 2021, shwfed.com
@Desc    :   时间日期工具测试
"""

# here put the import lib
from junglead import DatetimeUtil, PlusFormat
from datetime import datetime
import time
import unittest


class TestDatetime(unittest.TestCase):

    def test_timezone(self):
        # 1.获取时区
        _tzinfo1 = DatetimeUtil.get_timezone_asia()
        _tzinfo2 = DatetimeUtil.get_timezone(1)
        print(_tzinfo1, _tzinfo2)

    def test_compare(self):
        _tzinfo = DatetimeUtil.get_timezone_asia()
        _dt1 = datetime(2021, 10, 1, 10, 0, 0, tzinfo=_tzinfo)
        _dt2 = datetime(2025, 10, 18, 10, 0, 0, tzinfo=_tzinfo)
        print(DatetimeUtil.after_now(_dt1), DatetimeUtil.before_now(_dt1), DatetimeUtil.after_now(_dt2), DatetimeUtil.before_now(_dt2))
        print(DatetimeUtil.compare_date(_dt1, _dt2), DatetimeUtil.compare_date(_dt1, _dt1), DatetimeUtil.compare_date(_dt2, _dt1))

        _dt1 = datetime(2021, 10, 1, 10, 0, 0)
        _dt2 = datetime(2025, 10, 18, 10, 0, 0)
        print(DatetimeUtil.compare_date(_dt1, _dt2), DatetimeUtil.compare_date(_dt1, _dt1), DatetimeUtil.compare_date(_dt2, _dt1))

    def test_calculate(self):
        _dt1 = datetime(2021, 10, 1, 10, 0, 0)
        _dt2 = datetime(2021, 10, 2, 10, 0, 0)
        # 计算日期时间差
        print(DatetimeUtil.minus_datetime(_dt1, _dt2))
        # 根据枚举对象增加
        print(DatetimeUtil.plus_datetime(_dt2, 3, PlusFormat.DAY))
        # 根据粒度化数值增加
        print(DatetimeUtil.plus_datetime_material(_dt2, weeks=-1, days=1, hours=1, minutes=1))

    def test_format(self):
        _dt1 = DatetimeUtil.convert_from_str('2021-10-01', '%Y-%m-%d')
        _dt2 = DatetimeUtil.convert_from_str('2021-10-0110:01:01', '%Y-%m-%d%H:%M:%S')
        _dt3 = DatetimeUtil.convert_from_timestamp(1634095098, DatetimeUtil.get_timezone(7))
        print(_dt1, _dt2, _dt3)

        print(f'输出时间戳：{DatetimeUtil.get_timestamp(13)}')
        print(f'输出当前时间简单形式：{DatetimeUtil.get_current_simple_str(False)}, {DatetimeUtil.get_current_simple_str()}')
        print(f'将指定日期输出为标准字符串：{DatetimeUtil.to_standard_str(_dt2)}')
        print(f'将指定日期输出为中文标准字符串：{DatetimeUtil.to_Chinese_str(_dt2)}')

    def test_format_2(self):
        _dt = datetime.now()
        print(_dt.strftime('%Y-%m-%d %H:%M:%S'), time.strftime('%Y-%m-%d %H:%M:%S %Z', time.localtime()))


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(TestDatetime('test_format'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
