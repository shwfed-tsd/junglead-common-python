# -*- encoding: utf-8 -*-
"""
@File    :   test_regex.py
@Time    :   2021/09/30 17:05:47
@Author  :   Gary.Wang
@Version :   1.0
@Contact :   wangyijun@shwfed.com
@License :   (C)Copyright 1990 - 2021, shwfed.com
@Desc    :   正则表达式工具的测试
"""

# here put the import lib
from junglead import RegexUtil
import unittest


class TestRegex(unittest.TestCase):

    def test_email(self):
        _email = '147637484@qq.com'
        self.assertTrue(RegexUtil.is_email(_email))
        _emails = [_email, 'bashenandi@163.com']
        _r, _msg = RegexUtil.is_all_email(_emails)
        if not _r:
            print(_msg)
        else:
            self.assertTrue(_r)

    def test_mobile(self):
        # self.assertTrue(RegexUtil.is_mobile(''))
        # self.assertTrue(RegexUtil.is_idcard(''))
        self.assertTrue(RegexUtil.is_zipcode('210000'))
        self.assertTrue(RegexUtil.is_ip('8.133.174.225'))
        self.assertTrue(RegexUtil.is_account('上海外高桥', 5, 20))
        self.assertTrue(RegexUtil.is_strong_passwd('Ww123456'))
        self.assertTrue(RegexUtil.is_tax_number('91310000055867488J'))

        self.assertTrue(RegexUtil.is_integer('12355'))
        self.assertTrue(RegexUtil.is_float('1321.11'))

        self.assertTrue(RegexUtil.is_qq('147637484'))
        self.assertTrue(RegexUtil.id_date('2021-01-21'))


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(TestRegex('test_mobile'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
