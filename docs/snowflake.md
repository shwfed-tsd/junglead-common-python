# 雪花算法

对于大型的分布式应用，或者海量并且有关联数据的写入操作来说，对于任何数据库来说，用自增列做为唯一标识都不是一个好的选择。雪花算法正是为解决这一问题而生。

雪花算法的本质，实际就是生成一个唯一的 **64bit** 整数，对于不同的数据库可以用不同的长整型存储，例如：**bigint**、**long** 等。其生成规则为：
* 1bit：符号位，默认为0；
* 41bit：用来记录时间戳。即使按照1970年开始，也可以使用到2039年，而且还可以更改起始时间；
* 10bit：用来记录机器id，默认前5bit表示数据中心，后5bit表示机器id，默认支持  2^10 -1 ≈1023 台机器；
* 12bit：循环位，来对应1毫秒内产生的不同id，大概可满足1毫秒 2^12 - 1≈4095 个数值。

### 使用方法
```Python
from junglead import SnowflakeUtil

# 构造函数参数 datacenter_id 表示数据中心编号；worker_id 表示机器编号。原则上最大值均不超过 510 个
_util = SnowflakeUtil(datacenter_id, worker_id)
_ids = []
for i in range(10000):
	_ids.append(_util.get_id())
print(_ids)
```