# -*- encoding: utf-8 -*-
"""
@File    :   test_email.py
@Time    :   2021/09/30 17:05:17
@Author  :   Gary.Wang
@Version :   1.0
@Contact :   wangyijun@shwfed.com
@License :   (C)Copyright 1990 - 2021, shwfed.com
@Desc    :   电子邮件工具测试
"""

# here put the import lib
from junglead import EmailUtil
import unittest


class TestEmail(unittest.TestCase):

    def setUp(self):
        self._email_util = EmailUtil('smtpdm.aliyun.com', 80, 'info@mail1.shwfed.com', 'SHwfed1234', pop_server='pop.qq.com', pop_port=995)

    def test_send(self):
        _r, _msg = self._email_util.send(['wangyijun@shwfed.com'], '匠岭产品服务中心', '您有一封通知邮件', '这是通知内容')
        print(_msg)
        self.assertTrue(_r)

    def test_receive(self):
        _emails = self._email_util.receive("147637484@qq.com", "wfezhfbxfoiqcahe")
        print(_emails)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    # suite.addTest(TestEmail('test_send'))
    suite.addTest(TestEmail('test_receive'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
