# -*- encoding: utf-8 -*-
"""
@File    :   test_snowflake.py
@Time    :   2021/12/07 16:21:57
@Author  :   Gary.Wang
@Version :   1.0
@Contact :   wangyijun@shwfed.com
@License :   (C)Copyright 1990 - 2021, shwfed.com
@Desc    :   测试雪花算法
"""

# here put the import lib
from junglead import SnowflakeUtil
import unittest


class TestSnowflake(unittest.TestCase):

    def test_generator_id(self):
        _util = SnowflakeUtil(1, 1)
        _ids = []
        for i in range(10000):
            _ids.append(_util.get_id())
        print(_ids)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    # suite.addTest(TestEmail('test_send'))
    suite.addTest(TestSnowflake('test_generator_id'))
    runner = unittest.TextTestRunner()
    runner.run(suite)
